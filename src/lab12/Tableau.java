/**
 * 
 */
package lab12;

/**
 * @author jtsheke
 *
 */
import java.util.*;
public class Tableau {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		double[] tabSimple = {12, 15.98, 12.9, 45, 7.9, -3.8};
		double[][] matriceCarree = {
				                   {12.4, 15.98, 12.9, 45, 7.9, -7.68},
				                   {12, 1.98, 12.9, 5, 7.9, 3.1},
				                   {0.6, 15.98, 1.47, 54, 7.9, -3.8},
				                   {12, 100, 12.0, 40, 7.9, -3.8},
				                   {2, 15.98, 12.9, 4, 7.9, -3.8},
				                   {1.8, 15.98, 12.9, 45, 7.9, 43.8}
		                           };

		Tableau tabObj = new Tableau();
		double moyenneTab = tabObj.moyenneTableauSimple(tabSimple);
		double sommeDiagonale = tabObj.sommeDiagonaleMontante(matriceCarree);
		System.out.format("La moyenne du tableau simple est: %.4f%n",moyenneTab);
		System.out.format("La somme de la Diagonale Montante de la matrice carrée est: %.4f%n", sommeDiagonale);
	}
	
	double moyenneTableauSimple(double[] tab) {
		double moyenne = 0.0;
		double somme = 0.0;
		int nbElements = tab.length;
		for(double elem:tab){
			somme = somme + elem;
		}
		 if(nbElements>0){
			 moyenne = (somme / nbElements);
		 }else{
			 //ici on peut retourner une valeur invalide par défaut
			 //ou générer une erreur (Exception) 
			 // Si on veut énérer une exception alors  ajouter
			 //"throws Exception" à la déclaration de la méthode après les parenthèses
			 //et décommenter la ligne suivante
			 //throw new Exception("Tableau vide");
		 }
		return(moyenne);
	}
	
	double sommeDiagonaleMontante(double[][] tab){
		double sommeDiag = 0.0; //retournera 0 si tableau vide
		int dimmensionTab = tab[0].length;//tab est carré donc tous les cotés même longueur
		int ligne = dimmensionTab - 1; //dernière ligne
		for (int colonne=0; colonne <dimmensionTab; colonne++){
			sommeDiag = sommeDiag + tab[ligne][colonne];
			ligne = ligne - 1;//on remente d'une ligne
		}
		return(sommeDiag);
	}

}
