/**
 * Johnny Tsheke @UQAM --INF1256 --lab12
 */
package lab12;

import java.util.*;
public class Cours {

	static Map<String,String> repertoireCours = new HashMap<String,String>();
	public static void main(String[] args) {
		
		repertoireCours.put("INF1256", "Informatique pour les sciences de la gestion");
		repertoireCours.put("INF1120", "Programmation 1");
		repertoireCours.put("INF2005", "Programmation web");
		repertoireCours.put("INF3005", "Programmation web Avancée");
		Set<String> sectionActuelle = new HashSet<String>();
		sectionActuelle.add("INF1120");
		sectionActuelle.add("INF1256");
		Cours cours = new Cours();
		cours.afficheCoursSession(sectionActuelle);
		//décommenter les 2 lignes suivantes et observer l'affichage
		//sectionActuelle.add("INF3005");
		//cours.afficheCoursSession(sectionActuelle);
	}

	void afficheCoursSession(Set<String> ens){
		System.out.println("Voici les cours de la session");
		for(String sigle:ens){//Pour chaque sigle se trouvant dans l'ensemble
			String titre=repertoireCours.get(sigle);//on prend le titre du sigle
			System.out.format("%s %s%n",sigle,titre);
		}
	}
}
